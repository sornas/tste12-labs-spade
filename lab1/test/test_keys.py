# top=main::top

import logging

from cocotb.clock import Clock
from cocotb.triggers import Timer
from spade import *

def binary_pad(n: int, length: int) -> str:
    return bin(n)[2:].zfill(length)

async def _test_scancode(s, clk, scancode: str) -> str:
    # setup clock
    await cocotb.start(Clock(clk, 10, units='ns').start())
    # stimulate kb_data and kb_clk
    await Timer(25, 'us')
    s.i.kb_clk = "true"
    s.i.kb_data = "true"
    await Timer(75, 'us')
    # add start bit
    scancode += "0"
    # add parity (not part of design)
    scancode = "0" + scancode
    # add end bit
    scancode = "1" + scancode
    for i, b in enumerate(scancode[::-1], start=1):
        cocotb.log.info(f"sending bit {i}/{len(scancode)}")
        s.i.kb_data = "true" if b == "1" else "false"
        await Timer(25, 'us')
        s.i.kb_clk = "false"
        await Timer(50, 'us')
        s.i.kb_clk = "true"
        await Timer(25, 'us')
    # return output
    return binary_pad(int(s.o.value()), 7)

@cocotb.test()
async def test_key_4(dut):
    assert await _test_scancode(SpadeExt(dut), dut.clk_i, scancode="00100101") == "0011001"

@cocotb.test()
async def test_key_a(dut):
    assert await _test_scancode(SpadeExt(dut), dut.clk_i, scancode="00111000") == "0000110"
